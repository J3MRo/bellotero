export const INGREDIENT_SPENDING = 'INGREDIENT_SPENDING';
export const FULLTIME_EMPLOYEES = 'FULLTIME_EMPLOYEES';
export const FOOD_COST_SAVING = 'FOOD_COST_SAVING';
export const ANNUAL_SAVINGS = 'ANNUAL_SAVINGS';


export const ingredientSpending = (value) => {
    return{
        type: INGREDIENT_SPENDING,
        valing:value
    }
}

export const fulltimeEmployeess = (value) => {
    return{
        type: FULLTIME_EMPLOYEES,
        valemployees:value
    }
}

export const doFoodCost = ()=>{
    return{
        type:FOOD_COST_SAVING
    }
}

export const foodCostSaving = (value) => {
    return dispatch => {
        dispatch(ingredientSpending(value))
        setTimeout(() => {
            dispatch(doFoodCost());
        }, 1000);
    }
}

export const doAnnnualSave = ()=>{
    return{
        type:ANNUAL_SAVINGS
    }
}

export const annualSavings = (value) => {
    return dispatch => {
        dispatch(fulltimeEmployeess(value))
        setTimeout(() => {
            dispatch(doAnnnualSave());
        }, 1000);
    }
}