import * as actionTypes from '../actions/actions';

const initialState = {
    ing:10,
    employsP:1,
    foodSaving:10,
    employeesSaving:1
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INGREDIENT_SPENDING:
            return{
                ...state,
                ing:action.valing

            }
            
        case actionTypes.FULLTIME_EMPLOYEES:
            return{
                ...state,
                employsP:action.valemployees
            }
        
        case actionTypes.FOOD_COST_SAVING:
            return{
                ...state,
                foodSaving:state.ing * 0.3
            };

        case actionTypes.ANNUAL_SAVINGS:
            return{
                ...state,
                employeesSaving: (state.employsP * 1337) + state.foodSaving
            };
            
        default:
            return state;
    }

};

export default reducer;