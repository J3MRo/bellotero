import { ROUTE_HOME, ROUTE_SOLUTIONS } from '../routes';

const navbarLinks = [
    {
        path: ROUTE_HOME,
        name: "Home",
        id: 'home',
    },
    {
        path: ROUTE_SOLUTIONS,
        name: "Calculator",
        id: 'solutions',
    },
    {
        path: "",
        name: "Stories",
        id: 'stories',
    },
    {
        path: "",
        name: "Partners",
        id: 'partnes',
    },
    {
        path: "",
        name: "About",
        id: 'about',
    },
    {
        path: "",
        name: "Blog",
        id: 'blog',
   },
    
];

export { navbarLinks }