import React from 'react'
import Home from "../layouts/Home";
import { ROUTE_HOME, ROUTE_SOLUTIONS } from './routes';
import Solutions from '../layouts/Solutions';

export const indexRoutes = [
    { path: ROUTE_HOME, name: 'Home', component: (props) => <Home {...props} /> },
    { path: ROUTE_SOLUTIONS, name: 'Solutions', component: (props) => <Solutions {...props} /> },
];

