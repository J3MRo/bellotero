import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import { indexRoutes } from "./routes";
import NavbarMenu from './components/Navbarmenu'

import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import counterReducer from './store/reducers/counter';
import resultReducer from './store/reducers/result';

const rootReducers = combineReducers({
  amount: counterReducer,
  results: resultReducer
})

const logger = store =>{
  return next => {
    return action => {
      console.log('[Middleware] Dispatching', action);
      const result = next(action);
      console.log('[Middlaware] next state', store.getState());
      return result;
    }
  }
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducers, composeEnhancers(applyMiddleware(logger, thunk)));

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <BrowserRouter>
          <NavbarMenu />
            <Switch>    
            {indexRoutes.map((props, key) => {
                                    return <Route exact path={props.path} component={props.component} key={key} />; 
                            })}
            </Switch>
        </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
