import React, { Component, Fragment} from 'react';
import CalculatorText from '../components/Hooks/CalculatorText';
import Calculator from '../components/Calculator';

class Solutions extends Component {
    state = {  }
    render() { 
        return ( 
            <Fragment>
               <div id="solutions">
               <div className="container-fluid">
               <div className="solutions-container">
                    <div className="row">
                            <div className="col">
                                <CalculatorText/>
                            </div>
                            <div className="col">
                                <Calculator/>
                            </div>
                        </div>
                </div>
               </div>
               </div>
            </Fragment>
         );
    }
}
 
export default Solutions;