import React, { Component, Fragment} from 'react';
import Testimonials from '../components/Hooks/Testimonials'

class Home extends Component {
    state = {  }
    render() { 
        return ( 
            <Fragment>
               <div id="home">
                   <div className="container-fluid">
                       <div className="container-home">
                           <Testimonials/>
                       </div>
                   </div>

               </div>
                
            </Fragment>
         );
    }
}
 
export default Home;