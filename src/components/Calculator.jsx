import React, { Component, Fragment} from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../store/actions/actions';

class Calculator extends Component {
    state = { }

    // componentDidMount(){
    //     document.addEventListener('change', this.updateValue);
    // }

    // updateValue = (e) => {
    //     // alert(e.target.value)
    //     this.props.onIngSpend(e.target.value)
    //   }

    render() { 
        const ingredient = this.props.ings;
        const time = this.props.times;
        const foodCost = this.props.food;
        const annualSave = this.props.money
        return ( 
            <Fragment>
                <div className="row">
                    <div className="col">
                        <div className="calculator-titles">
                            <h6>Monthly<br/> ingredient spending</h6>
                        </div>
                    </div>
                    <div className="col">
                        <input 
                            className="calculator-text-input"
                            type="text" 
                            name="ingredientcost" 
                            value={ingredient}
                            onChange={(e) => this.props.onFoodSaving(e.target.value)}></input>
                    </div>
                </div>
                <div className="slider">
                        <input type="range" name="points" 
                            min="10" 
                            max="100"
                            value= {ingredient}
                            onChange={(e) => this.props.onFoodSaving(e.target.value)}/>
                </div>

                <div className="row claculator-container-employess">
                    <div className="col">
                        <div className="calculator-title">
                            <h6>Full-time employees that <br/> process invoices</h6>
                        </div>
                    </div>
                    <div className="col">
                        <input 
                            className="calculator-text-input2"
                            type="text" 
                            name="employeesprocess" 
                            pattern="\d*" 
                            maxLength="10"
                            value={time}
                            onChange={e => this.props.onAnnualSaving(e.target.value)}></input>
                    </div>
                </div>
                <div className="slider">
                        <input type="range" 
                            name="points" 
                            min="1" 
                            max="10"
                            value={time ? time:1}
                            onChange={e => this.props.onAnnualSaving(e.target.value)}/>
                </div>
                
                <div className="row">
                    <div className="col">
                        <span>${foodCost}</span><br/>
                        <span>Estimated cost food savings</span>
                    </div>
                    <div className="col">
                        <span>${annualSave}</span><br/>
                        <span>Your estimated annual savings</span>
                    </div>
                </div>
            </Fragment>
         );
    }
}

const mapStateToProps = state => {
    return {
        ings: state.amount.ing,
        times: state.amount.employsP,
        food: state.amount.foodSaving,
        money: state.amount.employeesSaving,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onIngSpend: (value) => dispatch(actionCreators.ingredientSpending(value)),
        onFullTime: (value) => dispatch(actionCreators.fulltimeEmployeess(value)),
        onFoodSaving: (value) => dispatch(actionCreators.foodCostSaving(value)),
        onAnnualSaving: (value) => dispatch(actionCreators.annualSavings(value))
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(Calculator);