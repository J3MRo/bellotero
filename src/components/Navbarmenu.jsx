/* eslint-disable no-self-compare */
import React, { Component, Fragment} from 'react'
import { Link } from "react-router-dom";
import { navbarLinks } from "../routes/menu";
import bellotero_logo from '../assets/menu/bellotero.svg'

class NavbarMenu extends Component {
    state = { active: 'home'}
    
    rendermenuLinks = (menuActive) => {
        return (
            navbarLinks.map((prop, key) => {
                let route = prop.path 
                route = route.replace(':projectId', this.props.projectId)
                return (
                    <li key={key} 
                        className={`nav-item ${menuActive === prop.id ? 'active': ''}`}
                        onClick={this.handlerStyles}>
                        <Link to={route} className="nav-link" id = {prop.id}>
                            {prop.name}
                        </Link>
                    </li>
                )
            })
        )
      }

      //add class active to a spesific link 
      handlerStyles = (e) =>{
         const clicked = e.target.id
        if(this.state.active === clicked) { 
            this.setState({active: ''});
        } else {
            this.setState({active: clicked})
       }
      }

    render() { 
        const menuActive = this.state.active ;
        return ( 
            <Fragment>
                <div  id="navbarNav">
                    <nav className="navbar ">
                        <Link className="navbar-brand" to="/">
                        <img src={bellotero_logo} alt="bellotero logo" />
                        </Link>
                        
                            <ul className="nav justify-content-end">
                                {this.rendermenuLinks(menuActive)}
                            </ul>
                        
                    </nav>
                </div>
            </Fragment>
         );
    }
}
 
export default NavbarMenu;