import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { Link } from "react-router-dom";

function Testimonials() {
    const [testimonialstitle, setTestimonialsTitle] = useState('')
    const [testimonialsReviews, setTestimonialsReviews]= useState([])
    const initCount = 0
    const [count,setCount] = useState(initCount)

    useEffect(() => {
        axios.get('https://raw.githubusercontent.com/Bernabe-Felix/Bellotero/master/page1.json')
            .then(res => {
                // console.log(res.data.slider)
                setTestimonialsTitle(res.data.slider)
                setTestimonialsReviews(res.data.slider.reviews)
                // console.log(res.data.slider.reviews.length);
                        
            })
            .catch(err => {
                console.log(err);
            })
    }, [])

    return (
        <div>
            <div className="home-testimonial-title">
                <h2>{testimonialstitle.title}</h2>
            </div>
            <div className="row testimonials-content">
                <div className="col">
                {testimonialsReviews && testimonialsReviews.length ? 
                   <div className="testimonial-ppl"> <h2>{testimonialsReviews[count].name} </h2><h6>{testimonialsReviews[count].position}</h6></div> :''}
                </div>
                <div className="col">
                    <p>
                    {testimonialsReviews && testimonialsReviews.length ? testimonialsReviews[count].comment:''}
                    </p>
                </div>
            </div>
            <div className="row testimonial-container-burttons">
                    <div className="col">
                        <span>{count + 1}/{testimonialsReviews.length}</span>
                    </div>
                    <div className={ count +1 < testimonialsReviews.length ? 'col arrows disabled': "col arrows"} 
                        onClick={() => setCount(count-1)} >
                        <Link className="arrow-link left" to="#"></Link>
                    </div>
                    <div className={ count + 2 > testimonialsReviews.length ? 'col arrows disabled': "col arrows"} 
                        onClick={() => setCount(count+1)} >
                        <Link className="arrow-link" to="#" ></Link>
                    </div>
            </div>       
        </div>
    )
}

export default Testimonials
