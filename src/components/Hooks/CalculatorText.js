import React, {useState, useEffect} from 'react'
import axios from 'axios'


function CalculatorText() {
    const [calculatorTxt, setCalculator] = useState('')

    useEffect(() => {
        axios.get('https://raw.githubusercontent.com/Bernabe-Felix/Bellotero/master/page2.json')
            .then(res => {
                // console.log(res.data.calculator)
                setCalculator(res.data.calculator)
                        
            })
            .catch(err => {
                console.log(err);
            })
    }, [])

    const split = calculatorTxt.title ? calculatorTxt.title.split(" "):" "
    const title1 = [split[0], split[1] , split[2]].join(" ")
    const title2 = [split[3]].join('')
    return (
        <div className="calculatorTxt-container">        
            <div className="calcutator-contain-title">
                <h2 className="title1">{title1}</h2>
                <h2 className="title2">{title2}</h2>
            </div>
            <p>{calculatorTxt.description}</p>    
        </div>
    )
}

export default CalculatorText
